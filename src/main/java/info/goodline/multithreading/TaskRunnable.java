package info.goodline.multithreading;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import info.goodline.model.RdrRaw;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Created by g on 08.11.15.
 */
public class TaskRunnable implements Runnable {
    private static final Logger log = Logger.getLogger(TaskRunnable.class);
    public static final String WRITE_TO_FILE_PACKET_SIZE_D = "write to file, packet size=%d";
    private Configuration conf;
    private Path hdfsFile;
    //private FileStatus[] files;
    private List<String> files;
    FileSystem local;
    int number;
    int mMaxPacketSize;

    public TaskRunnable(List<String> files, int number, Configuration conf
            , Path hdfsFile, int maxPacketSize) {
        log.setLevel(Level.ALL);
        this.conf = conf;
        this.files = files;
        this.hdfsFile = hdfsFile;
        this.number = number;
        mMaxPacketSize = maxPacketSize;
        try {
            local = FileSystem.getLocal(conf);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            int fileCounter = 0;
            StringBuilder packetSb = new StringBuilder();
            FileSystem hdfs = FileSystem.get(conf);
            BufferedWriter bw = null;
            long total = 0;
            StringBuilder sb = null;
            for (int i = 0; i < files.size(); i++) {

                if (bw == null) {
                    log.debug(String.format("Create new out file %s index = %d", hdfsFile.getName(), i));
                    bw = new BufferedWriter(new OutputStreamWriter(hdfs.create(hdfsFile, true)));
                }
                // читаем построчно текущий файл
                sb = new StringBuilder();
                BufferedReader br = new BufferedReader(new FileReader(files.get(i)));
                try {

                    String line = br.readLine();
                    while (line != null) {
                        //check line
                        if (!line.contains("TIME_STAMP")) {
                            //parse line
                            RdrRaw rdrRaw = RdrRaw.getInstance(line);
                            if (rdrRaw == null) continue;
                            if (rdrRaw.dstHost.length() > 0) {
                                //if host not empty
                                sb.append(rdrRaw.toStringLine());
                                sb.append(System.lineSeparator());
                            }
                        }
                        line = br.readLine();
                    }
                } finally {
                    br.close();
                }
                //check bufferSize
                if (packetSb.length() + sb.length() > mMaxPacketSize) {
                    log.debug(String.format(WRITE_TO_FILE_PACKET_SIZE_D, packetSb.length()));
                    bw.write(packetSb.toString());
                    packetSb = new StringBuilder();
                    bw.close();
                    bw = null;
                    fileCounter++;
                } else {
                    packetSb.append(sb.toString());
                    sb = null;
                }
            }

            if (sb != null && sb.length() > 0) {
                packetSb.append(sb.toString());
            }
            if (packetSb != null && packetSb.length() > 0) {
                log.debug(String.format(WRITE_TO_FILE_PACKET_SIZE_D, packetSb.length()));
                bw.write(packetSb.toString());
                fileCounter++;
                bw.close();
                bw = null;
            }
            String msg = String.format("Tread %d write %d\n", number, fileCounter);
            System.out.println(msg);
            log.debug(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
