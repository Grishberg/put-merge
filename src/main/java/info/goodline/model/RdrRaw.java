package info.goodline.model;

public class RdrRaw {
    public String dstHost;
    public String dstParam;
    public String serverIp;
    public String userId;
    public String userIp;
    public String info;
    public String date;

    public static RdrRaw getInstance(String string) {
        String[] c = string.split(",");
        if (c == null || c.length < 12) {
            return null;
        }
        return new RdrRaw(c);
    }

    private RdrRaw(String[] c) {
        this.serverIp = checkNull(c[8]);
        this.userId = checkNull(c[3]);
        this.userIp = checkNull(c[12]);
        this.dstHost = checkNull(c[10]);
        this.dstParam = checkNull(c[11]);
        this.date = checkNull(c[0]);
    }

    public String toStringLine() {
        return String.format("%s,%s,%s,%s,%s,%s"
                , date, serverIp, userId, userIp, dstHost, dstParam);
    }

    @Override
    public String toString() {
        return "serverIp = [" + serverIp + "], userIp = [" + userIp + "], info = [" + info + "]";
    }

    private String checkNull(String src) {
        return src == null ? "" : src;
    }
}

