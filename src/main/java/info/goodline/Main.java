package info.goodline;

import info.goodline.multithreading.TaskRunnable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocalFileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Created by g on 21.12.15.
 */
public class Main {
    private static final Logger log = Logger.getLogger(Main.class);
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int MEGABYTE = 1024 * 1024;
    public static final String THREAD_D_STARTED = "thread %d started";
    public static final String DOT_STR = ".";
    public static final String SLASH_STR = "/";
    private ExecutorService executor = null;
    private long[] mUpdateTime;
    private String mTimeStr;

    public Main(String inPath, String outPath, int blockSize, int threadsCount) throws IOException {
        log.setLevel(Level.ALL);
        System.out.println("PutMerge v2.1.5");
        System.out.printf("cores count = %d \n", CPU_COUNT);
        System.out.printf("threads count = %d \n", threadsCount);
        System.out.printf("blockSize = %d mb \n", blockSize);
        log.info(String.format("cores count = %d \n", CPU_COUNT));
        log.info(String.format("threads count = %d \n", threadsCount));
        log.info(String.format("blockSize = %d mb \n", blockSize));

        blockSize = blockSize * MEGABYTE;
        if (blockSize == 0) {
            blockSize = 128 * MEGABYTE;
        }
        executor = Executors.newFixedThreadPool(threadsCount);

        Configuration conf = new Configuration();
        FileSystem hdfs = FileSystem.get(conf);
        FileSystem local = FileSystem.getLocal(conf);
        final LocalFileSystem localFileSystem = FileSystem.getLocal(conf);

        Path inputDir = new Path(inPath);
        Path hdfsFile = new Path(outPath);

        // start cycle

        long startTime = System.currentTimeMillis();

        List<String> inputFiles = listFilesForFolder(new File(inPath));
        // инициализация массива
        List<String>[] files = new List[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            files[i] = new ArrayList<>();
        }

        int taskNumber = 0;
        // цикл по файлам в целевой папке
        mUpdateTime = new long[inputFiles.size() > 100000 ? inputFiles.size() : 100000];
        System.out.println("Start scanning files...");
        for (int i =0; i < inputFiles.size(); i++) {
            // индекс потока
            int threadIndex = (int) (i % threadsCount);
            String fn = inputFiles.get(i);
            File currentFile = new File(fn);
            long modifiedTime = currentFile.lastModified();
            int fileIndex = nameToInt(fn);
            if(fileIndex >=0 && modifiedTime > mUpdateTime[fileIndex]){
                mUpdateTime[fileIndex] = modifiedTime;
                files[threadIndex].add(fn);
            }

        }
        // запуск потоков на исполнение
        Calendar cal = Calendar.getInstance();
        mTimeStr = String.format("%04d%02d%02d%02d%02d%02d%03d",
                cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE)
        ,       cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND)
        ,       cal.get(Calendar.MILLISECOND) );

        for (int i = 0; i < threadsCount; i++) {
            log.debug(String.format(THREAD_D_STARTED, i));
            TaskRunnable taskRunnable = new TaskRunnable(
                    files[i]
                    , taskNumber
                    , conf
                    , hdfsFile.suffix(String.format("part.%s.%08d", mTimeStr, taskNumber++))
                    , blockSize);
            executor.submit(taskRunnable);
        }

        System.out.println("wait while run workers...");
        log.debug("wait workers...");
        //ожидание выполнения потоков
        executor.shutdown();
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // подсчет времени обработки
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        String msg = String.format("Done. running time = %d ms\n"
                , totalTime);
        log.debug(msg);
        System.out.println(msg);

        // можно добавить паузу и начать новый цикл
    }

    private int nameToInt(String fn){
        int res = -1;
        int dotPos = fn.lastIndexOf(DOT_STR);
        int slashPos = fn.lastIndexOf(SLASH_STR);
        if(slashPos >0 && dotPos >0){
            String fileNumber = fn.substring(slashPos+1, dotPos);
            if(fileNumber != null && fileNumber.length() >0){
                return Integer.valueOf(fileNumber);
            }
        }
        return res;
    }

    public List<String> listFilesForFolder(final File folder) {
        List<String> result = new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                result.add(fileEntry.getPath());
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int blockSize = 128;
        int threadsCount = 1;
        if (args.length < 4) {
            System.out.println("usage <in local> <out hdfs> <block size in MB> <threads count>");
            return;
        }
        try {
            threadsCount = Integer.valueOf(args[3]);
            blockSize = Integer.valueOf(args[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            new Main(args[0], args[1], blockSize, threadsCount);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
